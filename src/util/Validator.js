
/**
 * Validates number length
 * @param   SyntheticBaseEvent [event]
 */
export const validateNumberField = (e) =>{
    e.target.value = Math.max(0, parseInt(e.target.value) ).toString().slice(0,e.target.max.length)
}
