import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Loan from './components/loan/Loan';

ReactDOM.render(
  <React.StrictMode>
    <Loan />
  </React.StrictMode>,
  document.getElementById('root')
);


