import React, { Component } from "react";
import './Business.css';
import {validateNumberField} from '../../util/Validator';


/**
 * Get business form controls
 * @param   props [params]
 * @return div
 */
class Business extends Component {

    constructor(props) {
        super(props);
    }

    render(){
    return (
        <div className="col-sm">
            <h1>Business</h1>
            <div className="form-group">
                <label className="required" htmlFor="taxId">Tax id: </label>
                <input className="form-control" name="taxId" type="number" min = "0" max="99999" onInput = {validateNumberField}
                       required="true" onChange={this.props.handleChange} />
                <label className="required" htmlFor="name">Name: </label>
                <input className="form-control" name="name" maxLength="50" required="true" onChange={this.props.handleChange} />
            </div>
        </div>
    );
    }
}

export default Business;
