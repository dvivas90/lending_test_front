import React, {Component} from "react";
import './Owner.css';
import {validateNumberField} from '../../util/Validator';



/**
 * Get owner form controls
 * @param   props [params]
 * @return div
 */
class Owner extends Component {

    constructor(props) {
        super(props);
    }

    render(){
        return(
        <div className="col-sm">
            <h1>Owner</h1>
            <div className="form-group">
                <label className = "required" htmlFor="ssn">SSN: </label>
                <input className="form-control" name="ssn" type="number" min="0" max="9999999999"   onInput = {validateNumberField}  required="true" onChange={this.props.handleChange} />
                <label className = "required" htmlFor="name">Name: </label>
                <input className="form-control"name="name" maxLength="50" required="true" onChange={this.props.handleChange} />
                <label className = "required" htmlFor="email">Email: </label>
                <input className="form-control" name="email" type="email" required="true" onChange={this.props.handleChange} />
            </div>
        </div>
    );
    }
}

export default Owner;
