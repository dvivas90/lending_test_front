import React, { Component } from "react";
import './Loan.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Business from "../business/Business";
import Owner from "../owner/Owner";
import Request from "../request/Request";


class Loan extends Component {

    constructor(props) {
        super(props);

        this.state = {
            business: {
                taxId: "",
                name: ""
            },
            owner: {
                ssn: "",
                name: "",
                email: ""
            },
            request:{
              amount:"",
                state:""
            }
        };
        this.initHandlers();
    }

    /**
     * Initialize handlers
     */
    initHandlers() {
        this.handleChangeRequest = this.handleChangeRequest.bind(this);
        this.handleChangeBusiness = this.handleChangeBusiness.bind(this);
        this.handleChangeOwner = this.handleChangeOwner.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Bussiness change handler
     * @param {SyntheticBaseEvent} [e]
     */
    handleChangeBusiness (e) {
        let business = {...this.state.business}
        business[e.target.name]=e.target.value
        this.setState({business});
    }

    /**
     * Owner change handler
     * @param {SyntheticBaseEvent} [e]
     */
    handleChangeOwner (e) {
        let owner = {...this.state.owner}
        owner[e.target.name]=e.target.value
        this.setState({owner});
    }

    /**
     * Redquest change handler
     * @param {SyntheticBaseEvent} [e]
     */
    handleChangeRequest (e) {
        this.setState({request:{ [e.target.name]: e.target.value }});
    }

    updateRequestState (state)  {
        let request = {...this.state.request}
        request.state = state;
        this.setState({request})
    }

    postBusiness() {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.business)
        };
        fetch('/business', requestOptions)
            .then(response => response.text())
            .then(data =>  console.log('Business Saved OK'));
    }

    postOwner() {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.owner)
        };
        fetch('/owner', requestOptions)
            .then(response => response.text())
            .then(data =>  console.log('Owner Saved OK'));
    }


    requestLoan() {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.request)
        };
        fetch('/request', requestOptions)
            .then(response => response.text())
            .then(data =>  this.updateRequestState(data));
    }

    /**
     * Submit handler
     * @param {SyntheticBaseEvent} [e]
     */
    async handleSubmit (e) {
        e.preventDefault();
        await this.postOwner();
        await this.postBusiness();
        await this.requestLoan();
    }

    /**
     *The render function
     */
    render() {
        return (
            <div className="container">
            <form className="center" onSubmit ={this.handleSubmit}>
                <h1>Loan Form</h1>
                <div className="row">
                <Business handleChange = {this.handleChangeBusiness}/>
                <Owner handleChange = {this.handleChangeOwner}/>
                <Request handleChange = {this.handleChangeRequest} state = {this.state.request.state}/>
                </div>
                <input type='submit' value="Apply" />
            </form>
            </div>
        );
    }

}

export default Loan;
