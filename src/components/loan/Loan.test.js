import { render, screen } from '@testing-library/react';
import Loan from './Loan';

test('renders learn react link', () => {
  render(<Loan />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
