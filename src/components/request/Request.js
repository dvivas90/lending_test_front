import React, {Component} from "react";
import './Request.css';
import {validateNumberField} from '../../util/Validator';



/**
 * Get requests form controls
 * @param   props [params]
 * @return div
 */
class Request extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="col-sm">
                <h1>Request</h1>
                <div className="form-group">
                    <label className="required" htmlFor="amount">Amount: </label>
                    <input className="form-control" required="true" name="amount" type="number" max="99999999999"
                           onChange={this.props.handleChange}
                           onInput={validateNumberField}/>
                    <label htmlFor="name">State: </label>
                    <input className="form-control" name="state" value={this.props.state} readOnly="true"/>
                </div>
            </div>
        );
    }
}

export default Request;
